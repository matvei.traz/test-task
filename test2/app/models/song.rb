# frozen_string_literal: true

class Song < ApplicationRecord
  belongs_to :artist
  has_many :downloads, dependent: :destroy

  def self.top(days, count)
    joins(:downloads)
      .where('downloads.created_at >= ?', days.days.ago)
      .group('songs.id')
      .order('COUNT(downloads.id) DESC')
      .limit(count)
  end
end

# frozen_string_literal: true

class Artist < ApplicationRecord
  has_many :songs, -> { order(:title) }, dependent: :destroy

  def songs_top
    songs.joins('LEFT OUTER JOIN downloads ON downloads.song_id = songs.id')
         .select('songs.*, COALESCE(COUNT(downloads.id), 0) as downloads_count')
         .group('songs.id')
         .reorder('downloads_count DESC')
  end

  def self.top(letter, count)
    where('name LIKE ?', "#{letter}%")
      .joins(songs: :downloads)
      .group('artists.id')
      .order('COUNT(downloads.id) DESC')
      .limit(count)
  end
end

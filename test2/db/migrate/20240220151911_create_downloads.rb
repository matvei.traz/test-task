# frozen_string_literal: true

class CreateDownloads < ActiveRecord::Migration[7.1]
  def change
    create_table :downloads do |t|
      t.references :song, foreign_key: true, null: false

      t.timestamps
    end
  end
end

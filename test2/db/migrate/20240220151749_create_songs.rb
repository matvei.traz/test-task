# frozen_string_literal: true

class CreateSongs < ActiveRecord::Migration[7.1]
  def change
    create_table :songs do |t|
      t.string :title, null: false
      t.integer :length
      t.integer :filesize
      t.references :artist, foreign_key: true, null: false

      t.timestamps
    end
  end
end

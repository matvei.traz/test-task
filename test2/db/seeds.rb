# frozen_string_literal: true

baily = Artist.create(name: 'Baily')
billy = Artist.create(name: 'Billy')

Song.create(title: 'baily_song1', length: 100, filesize: 100, artist: baily)
Song.create(title: 'baily_song2', length: 1000, filesize: 200, artist: baily)
Song.create(title: 'baily_song3', length: 106, filesize: 300, artist: baily)
Song.create(title: 'baily_song4', length: 300, filesize: 400, artist: baily)

Song.create(title: 'billy_song1', length: 100, filesize: 100, artist: billy)
Song.create(title: 'billy_song2', length: 100, filesize: 100, artist: billy)
Song.create(title: 'billy_song3', length: 100, filesize: 100, artist: billy)
Song.create(title: 'billy_song4', length: 100, filesize: 100, artist: billy)

(1..2).each do |_|
  Download.create(song: Song.find_by(title: 'baily_song1'))
end

(1..4).each do |_|
  Download.create(song: Song.find_by(title: 'baily_song2'))
end

(1..5).each do |_|
  Download.create(song: Song.find_by(title: 'billy_song1'))
end

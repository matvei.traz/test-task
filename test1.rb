require 'bundler/inline'

gemfile do
  gem 'minitest'
end

require 'minitest/autorun'

def calculate_power(sentance)
  worlds_lengths = sentance.split.map(&:length)

  worlds_lengths.reverse.reduce do |acc, length|
    acc ** length
  end
end

if __FILE__ == $PROGRAM_NAME
  class CalculatePowerTest < Minitest::Test
    def test_calculate_power
      assert_nil calculate_power("")
      assert_equal 1, calculate_power("a")
      assert_equal 16, calculate_power("hi four")
      assert_equal 3125, calculate_power("hello world")
      assert_equal 3486784401, calculate_power("masha pila sok")
   end
  end
end
